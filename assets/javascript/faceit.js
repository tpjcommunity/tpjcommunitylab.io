---
layout: js_minifier
replace_names: false
---
import fetch from 'https://cdn.skypack.dev/cross-fetch';
// Import the faceit-wrapper library
import FaceitAPI from 'https://cdn.skypack.dev/@cstools-app/faceit-wrapper';

const k = '{{site.faceit}}';

const client = new FaceitAPI(k, fetch);

// should be loop so could retrieve all championships where tpj plays
const championshipIds = ['11e33072-ef2a-44f9-a1da-195f24e7bd79'];
const teamId = '14ba360e-7af7-417e-98c6-fd84b1d27cd5';

/*
 const __request = async(url, options) => {
    const headers = {
      'Content-Type': 'application/json',
    };

    return new Promise((resolve, reject) => {
      fetch(url, { ...options, headers })
        .then((res) => res.json())
        .then((json) => {
          resolve(json);
        })
        .catch((e) => reject(e));
    });
 };
*/

const formatTime = (unixTime) => {
  const timeOptions = {
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', timeZoneName: 'short', timeZone: 'Europe/Helsinki'
  };
  const date = new Intl.DateTimeFormat('en-GB',timeOptions).format(new Date(unixTime*1000));
  const formatted = {day: date.slice(0,2), month: date.slice(3,5), year: date.slice(6,10), time: date.slice(12,17), timeZone: date.slice(18,)};
  return formatted;
};

const splitStringOnNth = (str,delimiter,nthOccurance) => {
  let tokens = str.split(delimiter);
  let result = [tokens.slice(0, nthOccurance), tokens.slice(nthOccurance)].map(function(item) {
    return item.join(delimiter);
  });
  return result;
};

const createleague = () => {
  let leagues = '';
  console.log(championships.length);
  leagues += '<a href=' + championships.faceit_url.replace('{lang}', 'en') + '><img src="' + championships.organizer_data.avatar + '" data-aos="fade-up" data-aos-offset="100"  alt="" style="height:100px; width:100px;"></a>';
  document.getElementById('current-leagues').innerHTML = leagues;
};

const setRosterText = (html) => {
  return Object.assign(
    document.createElement('div'),
    { class: 'roster-text',
      innerHTML: html
    }
  );
};

const getroster = async () => {
  let offset = 10; // 0 dirty hack to be faster
  const limit = 10;
  let subscriptions = await client.championships.subscriptions({championship_id: championshipIds[0], offset: offset, limit: limit});
  let tpjObj = subscriptions.items.find((v) => v.team.name === 'Tpj');
  /*
   * while(tpjObj === undefined) {
   *   offset = offset + limit;
   *   subscriptions = await client.championships.subscriptions({championship_id: championshipIds[0], offset: offset, limit: limit});
   *   tpjObj = subscriptions.items.find((v) => v.team.name === 'Tpj')
   */
  let roster = '';
  let substitutes = '';

  for (let i = 0; i < tpjObj.team.members.length; i++) {
    const userid = tpjObj.team.members[i].user_id;
    let tmp_roster = '';
    let avatar = tpjObj.team.members[i].avatar;
    avatar = avatar.length > 0 ? avatar : 'assets/images/Rosterphoto.jpg';
    const name = tpjObj.team.members[i].nickname;
    const url = tpjObj.team.members[i].faceit_url.replace('{lang}', 'en');
//change thes to be DOMs. Thats how eventlistener can be added nice way, and can fetch the player stats
    const template = document.querySelector('#roster-element-template');
    const clone = template.content.cloneNode(true);
    let coverImg = clone.querySelector('.cover');
    coverImg.src = avatar;
    let rosterStats = clone.querySelector('.roster-stats');


    const faceitText = setRosterText('<p>FACEIT: <a href='+url+'>'+name+'</a></p>');
    const skillText = setRosterText('<p>Skill Level: ' + tpjObj.team.members[i].skill_level + '</p>');
    rosterStats.append(...[faceitText,skillText]);

    let rosterName = clone.querySelector('#roster-name');
    rosterName.innerHTML = '<p><b>' + name + '</b></p>';

    const rosterItem = clone.querySelector('.roster-item');
    rosterItem.addEventListener('click', () =>
      {
        setPlayerStats(name, userid, rosterStats);
      }, {once: true});

    const rosterWrap = document.getElementById('roster-wrap');
    const subsWrap = document.getElementById('substitute-wrap');
    if (tpjObj.roster.includes(userid)) {
        rosterWrap.appendChild(clone);
    } else {
        subsWrap.appendChild(clone);
    };



  };
  // -----------------Flippable cards-------------

  const flips = document.querySelectorAll('.flip');
  [...flips].forEach((card)=>{
    card.addEventListener('click', function() {
      card.classList.toggle('flipped');
    });
  });

  return tpjObj.team.members;
};

const setPlayerStats = async(playerName, playerId, element) => {
  const allStats = await getPlayerStats(playerName, playerId);
  const hltv1Rating = setRosterText('<p>HLTV 1.0: ' + allStats.hltv1  + '</p>');
  const elo = setRosterText('<p>ELO: ' + allStats.games.csgo.faceit_elo  + '</p>');
  const matches = setRosterText('<p>FACEIT Matches: ' + allStats.lifetime.Matches  + '</p>');
  const steamText = setRosterText('<p>Steam: <a href=https://steamcommunity.com/profiles/'+allStats.steam_id_64+'>'+allStats.steam_nickname+'</a></p>');
  element.append(...[hltv1Rating,elo,matches,steamText]);
};

const getPlayerStats = async(playerName, playerId) => {
  const [player, playerStats] = await Promise.all([await client.players.show({player_id: playerId, nickName: playerName, game: "csgo"}), await client.players.stats({player_id: playerId, game_id: "csgo"})]);

  let allStats = {...player, ...playerStats};
  allStats.hltv1 = calcHltv1(playerStats);
  return allStats;
};

//web.archive.org/web/20141031064500/www.hltv.org/?pageid=242&eventid=0
const calcHltv1 = (playerStats) => {
  const averageKPR = 0.679;
  const averageSPR = 0.317;
  const averageRMK = 1.277;
  let kills = 0;
  let kills3 = 0;
  let kills4 = 0;
  let kills5 = 0;
  let mkills = 0;
  let deaths = 0;
  let assits = 0;
  let rounds = 0;
  for (const seg of playerStats.segments) {
    kills  += Number(seg.stats.Kills);
    kills3 += Number(seg.stats["Triple Kills"]);
    kills4 += Number(seg.stats["Quadro Kills"]);
    kills5 += Number(seg.stats["Penta Kills"]);
    deaths += Number(seg.stats.Deaths);
    assits += Number(seg.stats.Assits);
    rounds += Number(seg.stats.Rounds);
  };
  mkills = kills - kills3*3 - kills4*4 - kills5*5;
  const killRating = kills/rounds/averageKPR;
  const survivalRating = (rounds - deaths)/rounds/averageSPR;
  const multiRating = (1.5*mkills + 9*kills3 + 16*kills4 + 25*kills5)/rounds/averageRMK;
  return ((killRating + 0.7*survivalRating + multiRating)/2.7).toFixed(2);
};

const getacademy = async (members) => {
  const teamTpj = await client.teams.show({team_id: teamId});
  let academy = '';

  for (let i = 0; i < teamTpj.members.length; i++) {
    const userid = teamTpj.members[i].user_id;
    let avatar = teamTpj.members[i].avatar;
    avatar = avatar.length > 0 ? avatar : 'assets/images/Rosterphoto.jpg';
    const name = teamTpj.members[i].nickname;
    const url = teamTpj.members[i].faceit_url.replace('{lang}', 'en');

    if (!members.find((e) => e.user_id === userid)) {
      academy += '<div class="roster-item" data-aos="flip-right" data-aos-offset="400">';
      academy += '<div class="roster-image"> <a href="' + url + '"> <img src="' + avatar + '" alt="" class="cover" width="190" height="290"> </a></div>';
      academy += '<div class="roster-text"><p>  <b>' + name + '</b></p></div>';
      academy += '</div>';
    }
  }
  const academywraps = document.querySelectorAll('[id="academy-wrap"]');
  for (let i = 0; i < academywraps.length; i++) {
    academywraps[i].innerHTML = academy;
  }
};

const getMatchesWithLimit = async(limit,type,api_id,id_id) => {
  let offset = 0;
  let tmpGames = await client.championships.matches({championship_id: api_id, type: type, limit: limit});
  let games = tmpGames.items;
  while (tmpGames.items.length > 0) {
    offset = offset + limit;
    tmpGames = await client.championships.matches({championship_id: api_id, type: type, limit: limit, offset: offset});
    games = games.concat(tmpGames.items);
  }
  games.filter((v,i,a)=>a.findIndex(v2=>(v2[id_id]===v[id_id]))===i);
  let tpjGames = games.filter((v) => (v.teams.faction2.name === 'Tpj' || v.teams.faction1.name === 'Tpj'));
  return tpjGames;
};

const getpast = (allGames) => {

  const pastGamesObj = allGames.filter((v) => (v.status === "FINISHED" || v.status === "CANCELLED"));
  const firstLine = '<li data-aos="flip-right" data-aos-anchor-placement="center-bottom">';
  let matchResult = ''; // variable to hold the new HTML data
  let matchColor = '';
  for (let i = 0; i < pastGamesObj.length; i++) {
    const unixTime = pastGamesObj[i].finished_at;
    const matchDay = formatTime(unixTime);
    const bestof = pastGamesObj[i].best_of;

    let gameText = matchDay.day + '.' + matchDay.month + ' | BO' + bestof + ' ' + pastGamesObj[i].teams.faction1.name + ' ' + pastGamesObj[i].results.score.faction1 + ' - ' + pastGamesObj[i].teams.faction2.name + ' ' + pastGamesObj[i].results.score.faction2;
    matchResult += createMapBlock(pastGamesObj[i], gameText, firstLine);
  }

  const resultsSplitted = splitStringOnNth(matchResult,firstLine,5);
  document.getElementById('results-list').innerHTML = resultsSplitted[0]; // add the new HTML data
  document.getElementById('results-hidden').innerHTML = resultsSplitted[1]; // add the new HTML data to the page
};

// Create a function that searches for a the championship
const get = (allGames) => {

  const upcomingGamesObj = allGames.filter((v) => v.status === "SCHEDULED");

  let newMatchUpcoming = '';
  let colorcodes = '';
  let upcoming = ''; // variable to hold the new HTML data
  const leagueColor = {};
  let tmp_leaguecolor = '';
  const firstLine = '<li data-aos="flip-right" data-aos-anchor-placement="center-bottom">';

  if (upcomingGamesObj.length === 0) {
    newMatchUpcoming += '<p><i>No Upcoming Matches :(</i></p>';
    document.getElementById('upcoming-list').innerHTML = newMatchUpcoming; // add the new HTML data to the page
  } else {
    for (let i = 0; i < upcomingGamesObj.length; i++) { // loop through first 8 matches
      if (upcomingGamesObj[i].competition_name.includes('FEL')) { // check if the league is FEL , Challengermode or Something else
        tmp_leaguecolor = '#2466FF';
        leagueColor.FEL = tmp_leaguecolor;
      } else if (upcomingGamesObj[i].competition_name.includes('Challengermode')) {
        tmp_leaguecolor = 'Orange';
        leagueColor.Challangermode = tmp_leaguecolor;
      } else if (upcomingGamesObj[i].competition_name.includes('eSM')) {
        tmp_leaguecolor = 'LightBlue';
        leagueColor.eSM = tmp_leaguecolor;
      } else {
        tmp_leaguecolor = '#b3b3b3';
        leagueColor.Other = tmp_leaguecolor;
      }

      const unixTime = upcomingGamesObj[i].scheduled_at;
      const matchDay = formatTime(unixTime);
      // constructing the html with the json data
      newMatchUpcoming += firstLine;
      newMatchUpcoming += '<button class="match-item" style="background:' + tmp_leaguecolor + ' url(' + championships.organizer_data.avatar + '); background-position: 10px 15px; background-repeat: no-repeat; background-size: 25px">';
      newMatchUpcoming += matchDay.day + '.' + matchDay.month + ' | ' + upcomingGamesObj[i].teams.faction1.name + ' vs ' + upcomingGamesObj[i].teams.faction2.name + ' ';
      newMatchUpcoming += '</button>';
      newMatchUpcoming += '<div class="match-info upcoming">';
      newMatchUpcoming += '<b>' + 'League:' + ' ' + upcomingGamesObj[i].competition_name + '</b><br>';
      newMatchUpcoming += '<b>' + 'BO: ' + upcomingGamesObj[i].best_of + '</b><br>';
      newMatchUpcoming += '<b>' + 'Match day: ' + matchDay.day + "." + matchDay.month + "." + matchDay.year + '</b><br>';
      newMatchUpcoming += '<b>' + 'Starting time: ' + matchDay.time + " " + matchDay.timeZone + '</b> <br>';
      newMatchUpcoming += '<b>' + 'Round: ' + upcomingGamesObj[i].round + ' Group: ' + upcomingGamesObj[i].group + '</b><br>';
      newMatchUpcoming += '</div>';

      upcoming = newMatchUpcoming + upcoming;
      newMatchUpcoming = '';
    }
    for (const key in leagueColor) {
      colorcodes += '<div class="square" style="background-color:' + leagueColor[key] + ';" ></div>';
      colorcodes += '<p>' + key + '</p>';
    }
    document.getElementById('colorcodes').innerHTML = colorcodes;
    const upcomingSplitted = splitStringOnNth(upcoming,firstLine,5);
    document.getElementById('upcoming-list').innerHTML = upcomingSplitted[0]; // add the new HTML data to the page
    document.getElementById('upcoming-hidden').innerHTML = upcomingSplitted[1]; // add the new HTML data to the page
  }
};

const getCurrent = (allGames) => {
  let closespan = document.getElementsByClassName("close-modal")[0];
  let modal = document.getElementById("my-modal");
  let modalText =  document.getElementsByClassName("modal-text")[0];
  let onGoing = {};

  const closemodal = () => {
    modal.style.display = "none";
  };

  //Possible statuses(?) for ongoing games
  const onGoingOpts = ["CHECK_IN","SUBSTITUTION","CAPTAIN_PICK","VOTING","CONFIGURING","READY","ONGOING","MANUAL_RESULT","PAUSED","ABORTED"];

  const current = allGames.find((v) => onGoingOpts.includes(v.status));
  if (current && Object.keys(current).length !== 0 && Object.getPrototypeOf(current) === Object.prototype ) {
    let innerText = '<b>' + 'Match going on! Check the stream!'+ '</b><br>';
    innerText +='<a href="#stream-header">'+ "Match: " + current.teams.faction2.name + ' vs ' + current.teams.faction1.name +"</a>";
    modalText.onclick = closemodal;

    modalText.innerHTML = innerText;
    modal.style.display = "block";

    onGoing = current;
  };

  // When the user clicks on <span> (x), close the modal
  closespan.onclick = closemodal;

// When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    };
  };
  return onGoing;
};

const createOnGoing = (match) => {

  let currentMatch = '';
  if (match.status === "ONGOING") {
    let firstLine = '<li data-aos="fade-in" data-aos-anchor-placement="center-bottom">';
    let upText = "Click to see the score of the on going match";
    currentMatch += createMapBlock(match, upText, firstLine);
    document.getElementById("ongoing-result").style.display = "block";
    document.getElementById("ongoing-result").innerHTML = currentMatch;
  };
};

const getSetMatchResult = async(matchId) => {

  document.getElementById("btn_"+matchId).onclick = ""; // we only want to get this once
  const match = await client.matches.stats({match_id: matchId});
  const matchRounds = match.rounds;
  for (let i=0; i<matchRounds.length; i++) {
    const mappick = matchRounds[i].round_stats.Map;
    const resultString = matchRounds[i].teams[0].team_stats.Team + " " + matchRounds[i].teams[0].team_stats["Final Score"] + '  |  ' + mappick + '  |  ' + matchRounds[i].teams[1].team_stats["Final Score"]
+ " " + matchRounds[i].teams[1].team_stats.Team;
    document.getElementById("h3_"+matchId+"_"+i).innerText = resultString;
  }
};

const getSetOnGoingResult = async(matchId) => {

  let resultString = "";
  const match = await client.matches.show({match_id: matchId});
  const mappick = match.voting ? match.voting.map.pick : []; // array of picked maps
  if (mappick.length > 0) {
    for (let i = 0; i < mappick.length; i++) {
      const mapelem = match.voting.map.entities.find((element) => element.game_map_id === mappick[i]);
      resultString = match.teams.faction1.name + ' ' + match.results.score.faction1 + '  |  '  + mapelem.class_name  + '  |  ' + match.results.score.faction2 + ' ' + match.teams.faction2.name;
      document.getElementById("h3_"+matchId+"_"+i).innerText = resultString;

    }
  }
  document.getElementById("btn_"+matchId).disabled = true; // set cooldown period for the button
  if (match.status==="ONGOING") {
    setTimeout(function(){
      document.getElementById("btn_"+matchId).disabled = false;
    },1500);
  };
};


const createMapBlock = (match, upText, firstLine) => {

  let newMatchResult = ''; // variable to hold the new HTML data
  let matchColor = '';
  const url = match.faceit_url.replace('{lang}', 'en');
  const winner = match.results.winner?.length > 0 ? match.results.winner : "no winner";
  const winnerObj = match.teams[winner];
  if (winnerObj) {
    if (winnerObj.name === 'Tpj') { // check if the outCome is win or loss
      matchColor = 'chartreuse';
    } else {
      matchColor = 'lightcoral';
    };
  } else {
    matchColor = 'darkgray';
  };
  newMatchResult += firstLine; // constructing the html with the json data
  if (match.status ==="ONGOING") {
    newMatchResult += '<button id=btn_'+match.match_id+' class="match-item" onclick="getSetOnGoingResult(\\''+ match.match_id +'\\')" style="background:' + matchColor + ' url(' + championships.organizer_data.avatar + '); background-position: 10px 15px; background-repeat: no-repeat; background-size: 25px">'; //two escapes, one for minifier and one for js
  } else {
    newMatchResult += '<button id=btn_'+match.match_id+' class="match-item" onclick="getSetMatchResult(\\''+ match.match_id +'\\')" style="background:' + matchColor + ' url(' + championships.organizer_data.avatar + '); background-position: 10px 15px; background-repeat: no-repeat; background-size: 25px">';
  };
  newMatchResult += upText;
  newMatchResult += '</button>';

  const mappick = match.voting ? match.voting.map.pick : []; // array of picked maps
  if (mappick.length > 0) {
    for (let j = 0; j < mappick.length; j++) {
      const mapelem = match.voting.map.entities.find((element) => element.game_map_id === mappick[j]);
      newMatchResult += '<div id="mapdiv_'+match.match_id+'_'+j+'" class="match-info" style="background-image: url(' + mapelem.image_lg + ');">';
      newMatchResult += '<a href="' + url + '"></a>';
      newMatchResult += '<h3 id=h3_'+match.match_id+'_'+j+'>' + match.teams.faction1.name + ' ' + match.results.score.faction1 + '  |  '  + mapelem.class_name  + '  |  ' + match.results.score.faction2 + ' ' + match.teams.faction2.name + '</h3></div>';
    }
  } else {
      newMatchResult += '<div class="match-info" style="background-image: url(' + winnerObj.avatar + ');">';
      newMatchResult += '<a href="' + url + '"></a>';
      newMatchResult += '<h3>' + winnerObj.name  + '</h3>' + '</div>';
  };
  return newMatchResult;
};

let onGoing = {};
const limit = 100;
const type = "all";
const champ_id = championshipIds[0];
const match_id = "match_id";

const [championships, members, allChampMatches] = await Promise.all([await client.championships.show({championship_id: champ_id, expanded: ['organizer']}), getroster(), getMatchesWithLimit(limit, type, champ_id, match_id)]);

createleague();
get(allChampMatches);
getpast(allChampMatches);
onGoing = getCurrent(allChampMatches);

if (onGoing.match_id) {
  createOnGoing(onGoing);
}

window.getSetMatchResult = getSetMatchResult;
window.getSetOnGoingResult = getSetOnGoingResult;

await Promise.all([getacademy(members)]);


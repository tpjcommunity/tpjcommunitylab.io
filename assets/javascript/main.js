$(() => { // On page load function
  $(window);
  $('#tpjlogo').addClass('spinner');

  setTimeout(() => {
    document.getElementById('tpjlogo').classList.remove('spinner');
    // $('html').css("background", 'url(assets/css/de_train_sumea.jpg) no-repeat center center');
  }, 3000);

  // -----------------NAVBAR ONSCROLL------------------

  window.onscroll = function() {
    myFunction();
  };

  const header = document.getElementById('nav-wrapper');
  const sticky = header.offsetTop;

  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add('fixed');
    } else {
      header.classList.remove('fixed');
    }
  }

  // ----------------- Copyright year -----------------
  document.getElementById('copyright').innerHTML = new Date().getFullYear();

  // ------------------TWITCH EMBEDDED-------------------
  /*
  document.getElementById('twitch-embed').innerHTLM = new Twitch.Embed("twitch-embed", {
        width: '95%',
        height: 480,
        channel: "tpjcommunity",
        theme: 'light'
  });
  */
  // ----------------Animate On Scroll------------

  AOS.init({
    duration: 800,
    once: true,
  });

  // -----------------FADE IN IMAGE---------------

  /* $('#canvas-img').ready(function(){
    $('#logo').fadeIn(0000);
  }); */

  // --------------------SMOOTH SCROLL----------------------------------------

  // Select all links with hashes
  $('a[href*="#"]')
  // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
      // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
        location.hostname == this.hostname
        ) {
        // Figure out element to scroll to
          let target = $(this.hash);
          target = target.length ? target : $(`[name=${this.hash.slice(1)}]`);
          // Does a scroll target exist?
          if (target.length) {
          // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top,
            }, 1000, () => {
            // Callback after animation
            // Must change focus!
              const $target = $(target);
              $target.focus();
              if ($target.is(':focus')) { // Checking if the target was focused
                return false;
              }
              $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
              $target.focus(); // Set focus again
              $target.css('outline', '0');
            });
          }
        }
      });
});

// ------------------------ MATCH LIST MORE INFO --------------------------------------------
$('.match-list').on('click', '.match-item', function(e) {
  e.preventDefault();
  $(this).nextAll('.match-info').not('animated').slideToggle();
});

// -------------------------SHOW MORE BUTTON---------------------------------
function showMore(obj,idName) {
  const $matches = $('#' + idName);
  const $showMoreText = $(obj).children('.show-more-text');
  const $arrowUp = $(obj).children('.up');
  const $arrowDown = $(obj).children('.down');

  if ($matches.is(':hidden')) {
    $matches.slideDown();
    $showMoreText.text('Hide');
    $arrowDown.hide();
    $arrowUp.show();
  } else {
    $matches.slideUp();
    $showMoreText.text('Show');
    $arrowUp.hide();
    $arrowDown.show();
  }
}
// ------------------------- MOBIE NAV --------------------------------------
function toggleNav() {
  const nav = document.getElementById('mySidenav');
  if (nav.style.height == '100%') {
    nav.style.height = '0';
  } else {
    nav.style.height = '100%';
  }
}

// ------------------------- Hide Mobile Nav --------------------------------
function hideNav() {
  const nav = document.getElementById('mySidenav');
  nav.style.height = '0';
}
